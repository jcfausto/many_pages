# README #

In order to get Many Pages up and running, you will need the folowing setup:

### What is this repository for? ###

This repo is the Many Pages iPhone App official repository.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

The app requires the XCode 6.3 or above, and the target is for 8.3 SDK and Swift 1.2

### Third Party Libraries ###

- Facebook SDK
- Parse SDK
- Jawbone JBChart

### Contribution guidelines ###

* Writing tests
* Code review
* Document your code

### Who do I talk to? ###

* Julio Cesar Fausto (jcfausto@gmail.com)