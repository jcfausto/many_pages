//
//  JBChart.h
//  JBChart
//
//  Created by Julio Cesar Fausto on 4/8/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JBChart.
FOUNDATION_EXPORT double JBChartVersionNumber;

//! Project version string for JBChart.
FOUNDATION_EXPORT const unsigned char JBChartVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JBChart/PublicHeader.h>

#import "JBChartView.h"
#import "JBBarChartView.h"
#import "JBChartInformationView.h"

