//
//  Parse-Bridge.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 3/31/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//
//  This is the bridging header for using Parse SDK with Swift

#import <Parse/Parse.h>