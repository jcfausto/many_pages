//
//  MPDataSourceProtocol.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/14/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation

protocol MPDataSourceProtocol {
    
    //The datasource type
    var type: NSCalendarUnit { get set }
    
    //The current data
    var data: [Int] { get set }
    
    //The query's range of dates that will be used as a filter for loading data
    var queryStartDate: NSDate { get set }
    var queryEndDate: NSDate { get set }
    
    /**
        Must return the quantity of records in the dataset
    */
    func getCount() -> Int
    
    /**
        Must return the sum of all record's values in the dataset
    */
    func getTotal() -> Int
    
    /**
        Must return the average calculated by the getTotal() / getCount()
    */
    func getAverage() -> CGFloat
    
    /**
        Must populate the datasource with data
    */
    func loadData()
    
    /**
        Must set the value of an specific record in the datasource
    */
    func setValueAtIndex(value: NSInteger, index: Int)
    
    /**
        Must retrieve the value of an specific record in the datasource
    */
    func getValueAtIndex(index: Int) -> Int
    
    /** 
        Custom initialization
    */
    func initDataSource()
    
    /**
        Proccess the data after loading
    */
    func processData(objects: [AnyObject]?)
}
