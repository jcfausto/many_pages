//
//  ThirdViewController.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 3/30/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet var userNameLabel: UILabel!
    
    @IBOutlet var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Define a custom background color
        self.view.backgroundColor = Constants.MainScreenBackgroundColor
        
        let versionNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
        let buildNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
        
        self.versionLabel.text = "V \(versionNumber)"
        
        //infoDictionary[@"CFBundleVersion"];
        
        if UserUtils.loggedIn() {
            
            var username: AnyObject? = PFUser.currentUser()!.username
            if (username != nil) {
                self.userNameLabel.text = (username as! String)
            }
            
            
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Perform a segue to the App's Main Screen
    func goToLoginScreen() {
        self.performSegueWithIdentifier(Constants.Segues.LoginScreen, sender: self)
        
    }
    

    @IBAction func logout(sender: AnyObject) {
        if Reachability.isConnectedToNetwork() {
            UserUtils.doLogout()
            self.goToLoginScreen()
        } else {
            let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                "\(Constants.ValidationMessages.NoConnectivity)", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
}

