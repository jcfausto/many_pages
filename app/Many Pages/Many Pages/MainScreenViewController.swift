//
//  FirstViewController.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 3/30/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import UIKit
import JBChart

var readingRecord: PFObject!

class MainScreenViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var pagesInputField: UITextField!
    @IBOutlet var thisWeekPageCountLabel: UILabel!
    @IBOutlet var barChartView: JBBarChartView!
    @IBOutlet var informationView: JBChartInformationView!
    @IBOutlet var informationViewLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Define a custom background color
        self.view.backgroundColor = Constants.MainScreenBackgroundColor
        
        //To perform correct keyboard handling
        self.pagesInputField.delegate = self
        
        self.informationView.backgroundColor = Constants.MainScreenBackgroundColor
        
        //Set up the barchart component
        
        self.barChartView.minimumValue = Constants.Jawbone.minimumValue
        self.barChartView.dataSource = WeekDataSource.sharedInstance
        self.barChartView.delegate = WeekBarChartDelegate.sharedInstance
        self.barChartView.backgroundColor = Constants.Jawbone.barChartBackgroundColor
        self.barChartView.footerPadding = Constants.Jawbone.defaultFooterPadding
        
    }
    
    override func viewWillAppear(animated: Bool) {
        if (UserUtils.loggedIn() && Reachability.isConnectedToNetwork()) {
            
            dispatch_async(dispatch_get_global_queue(
                Int(QOS_CLASS_USER_INTERACTIVE.value), 0)) {
                    
                    WeekDataSource.sharedInstance.loadData()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.barChartView.reloadData()
                        self.setPageCountForThisWeek(WeekDataSource.sharedInstance.getTotal())
                        self.setPageCountTo(WeekDataSource.sharedInstance.getTodaysPageCount())
                        
                    }
                    
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        WeekBarChartDelegate.sharedInstance.setInformationView(self.informationView, anInformationViewLabel: self.informationViewLabel)
        
        self.informationView.setHidden(true, animated: false)
        
        self.barChartView.footerView = self.getBarChartFooterViewFor(Constants.thisWeek, barChartWidth: self.barChartView.availableWidth(), barChartPaddingBetweenBars: Constants.Jawbone.defaultPaddingBetweenBars)
        
        self.barChartView.headerView = self.getBarChartHeaderViewFor(Constants.thisWeek, barChartWidth: self.barChartView.availableWidth())
        
        
    }
    
    func getBarChartFooterViewFor(time: Int, barChartWidth: CGFloat, barChartPaddingBetweenBars: CGFloat) -> UIView {
        
        var footerView = UIView(frame: CGRectMake(0, 0, barChartWidth, 16))
        
        let yPosition = 0
        
        //This is the x-position relative to the footerView, and not to the barChartView
        var xOffset: CGFloat = 0
        
        var barPadding: CGFloat!
        let footerLabelWidth: CGFloat!
        
        switch time {
            case Constants.thisWeek:
                barPadding =  ((1/7) * 50)
                footerLabelWidth = ((barChartWidth-(6*barPadding))/CGFloat(7))
                for weekDay in 1...7 {
                    var footerLabel = UILabel(frame: CGRectMake(xOffset, 0, footerLabelWidth, Constants.Jawbone.barChartFooterTextHeight))
                    footerLabel.textColor = UIColor.whiteColor()
                    footerLabel.text = Constants.weekSymbols[weekDay-1] as String
                    footerLabel.textAlignment = NSTextAlignment.Center
                    footerLabel.font = UIFont.systemFontOfSize(12)
                    footerLabel.textColor = Constants.Jawbone.barChartFooterTextColor
                    
                    footerView.addSubview(footerLabel)
                    
                    xOffset = CGRectGetMaxX(footerLabel.frame) + barPadding
                    
                }
            case Constants.thisMonth:
                barPadding =  ((1/12) * 50)
                footerLabelWidth = ((barChartWidth-(11*barPadding))/CGFloat(12))
                for month in 1...12 {
                    var footerLabel = UILabel(frame: CGRectMake(xOffset, 0, footerLabelWidth, Constants.Jawbone.barChartFooterTextHeight))
                    footerLabel.textColor = UIColor.whiteColor()
                    footerLabel.text = Constants.monthSymbols[month-1] as String
                    footerLabel.textAlignment = NSTextAlignment.Center
                    footerLabel.font = UIFont.systemFontOfSize(12)
                    footerLabel.textColor = Constants.Jawbone.barChartFooterTextColor
                    
                    footerView.addSubview(footerLabel)
                    
                    xOffset = CGRectGetMaxX(footerLabel.frame) + barPadding
                }
            case Constants.thisYear:
                barPadding =  ( 1 * 50 )
                footerLabelWidth = ((barChartWidth-(1*barPadding)))
                var footerLabel = UILabel(frame: CGRectMake(xOffset, 0, footerLabelWidth, Constants.Jawbone.barChartFooterTextHeight))
                footerLabel.textColor = UIColor.whiteColor()
                footerLabel.text = Constants.ThisYearLabel
                footerLabel.textAlignment = NSTextAlignment.Center
                footerLabel.font = UIFont.systemFontOfSize(12)
                footerLabel.textColor = Constants.Jawbone.barChartFooterTextColor
                
                footerView.addSubview(footerLabel)
            default:
                barPadding = 0
                footerLabelWidth = barChartWidth
        }
        
        return footerView
        
    }
    
    func getBarChartHeaderViewFor(time: Int, barChartWidth: CGFloat) -> UIView {
        //Bar chart header
        var header = UILabel(frame: CGRectMake(0, 0, barChartWidth, 50))
        
        header.textColor = Constants.Jawbone.barChartFooterTextColor
        
        header.font = UIFont.systemFontOfSize(15)
        
        header.text = "\(Constants.PagesLabel) \(Constants.timeLabels[time])"
        
        header.textAlignment = NSTextAlignment.Center
        
        return header as UIView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPageCountForThisWeek(number: NSInteger) {
        self.thisWeekPageCountLabel.text = number.description
    }
    

    // Keyboard handling routines
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.pagesInputField.resignFirstResponder()
        
        return true
    }
    
    // Users are allowed only to input integer values. Also check if the length, that support only 3 digits.
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = count(textField.text) + count(string) - range.length
        
        //To allow backspace
        if string.isEmpty {
            return true
        }
        
        //Only permit numbers
        switch string {
            case "0","1","2","3","4","5","6","7","8","9":
                return (newLength <= 3)
        default:
            return false
        }
        
    }
    
    //end of keyboard handling routines
    

    func setPageCountTo(value: NSInteger) {
        self.pagesInputField.text = value.description
    }
    
    
    //This action is fired every time a support button is pressed. 
    //The support buttons are those who have numbers on it.
    @IBAction func SupportButtonClick(sender: AnyObject) {
        self.setPageCountTo(sender.tag as NSInteger!)
    }

    
    @IBAction func saveRecord(sender: AnyObject) {
        
        if Reachability.isConnectedToNetwork() {
        
            if !self.pagesInputField.text.isEmpty {
                
                var pageCount: NSInteger = (self.pagesInputField.text as NSString).integerValue
                
                if pageCount > 0 {
                    
                    if pageCount <= 450 {
                
                        self.pagesInputField.resignFirstResponder()
                    
                        UserUtils.saveRecordForTodayWith(pageCount)
                        
                        WeekDataSource.sharedInstance.setValueAtIndex(pageCount, index: Constants.currentWeekDay-1)
                        
                        self.barChartView.reloadData()
                        
                        //Get the week's total page count
                        self.setPageCountForThisWeek(WeekDataSource.sharedInstance.getTotal())
                    } else {
                        let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                            "\(Constants.ValidationMessages.MaxPageInputExceeded)", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                    
                } else {
                    let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                        "\(Constants.ValidationMessages.InformANumberGreaterThanZero)", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
                
            } else {
                let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                    "\(Constants.ValidationMessages.InformANumberInOrdeToSave)", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        } else {
            
            let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                "\(Constants.ValidationMessages.NoConnectivity)", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
}

