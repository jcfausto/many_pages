//
//  LastWeekDataSource.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/22/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation

class LastWeekDataSource: MPDataSource {
    
    //Everytime that the datasource needs to be accessed, it must be done
    //by accessing this variable
    static let sharedInstance = LastWeekDataSource()
    
    override init() {
        super.init()
        let date = NSDate()
        self.type = NSCalendarUnit.CalendarUnitWeekday
        self.queryStartDate = date.beginningOfLastWeek()
        self.queryEndDate = date.endOfLastWeek()
        self.initDataSource()
    }
    
    override func initDataSource() {
        self.data = [0, 0, 0, 0, 0, 0, 0]
    }
    
    override func processData(objects: [AnyObject]?) {
        
        if objects != nil {
            
            var pCount: NSInteger = 0
            
            for (index, object) in enumerate(objects!) {
                
                var createdAt = object.valueForKey("createdAt") as! NSDate
                let aCalendar = NSCalendar.currentCalendar()
                let aComponents = aCalendar.components(NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekday, fromDate: createdAt)
                
                pCount = object.valueForKey("page_count") as! NSInteger
                self.setValueAtIndex(pCount, index: aComponents.weekday-1)
                
            } // end for
            
        } //end if
    }
    
}

