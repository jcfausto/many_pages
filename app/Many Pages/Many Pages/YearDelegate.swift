//
//  YearDelegate.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/15/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation

class YearDelegate: MPDelegate {
    
    //Everytime that the datasource needs to be accessed, it must be done
    //by accessing this variable
    static let sharedInstance = YearDelegate()
    
    override init() {
        super.init()
        self.datasource = YearDataSource.sharedInstance
    }

}