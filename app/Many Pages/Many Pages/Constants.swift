//
//  Constants.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 3/31/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//
// Sample Usage: Constants.Parse.Dev.ApplicationId

import Foundation

//Constants
struct Constants {
    
    
    //General
    static let UserLastLoginDate = "last_login"
    static let FirstDayLabel = "FIRST DAY"
    static let LastDayLabel = "LAST DAY"
    static let ThisYearLabel = "THIS YEAR"
    static let PagesLabel = "PAGES"
    static let ManyPagesLabel = "Many Pages"
    
    //Validation messages
    struct ValidationMessages {
        static let MaxPageInputExceeded =
    "Wonderful! Unfortunately, the maximum number of pages that could be saved by day is 450 pages."
        static let InformANumberGreaterThanZero = "Please, inform a number greater than 0."
        static let InformANumberInOrdeToSave = "Please, inform the number of pages in order to save."
        static let OKIGotIt = "Ok. I got it!"
        
        static let NoConnectivity = "The Internet connection appears to be offline. Check your connectivity and try again."
        
        static let EmailOrPasswordNotInformed = "Please enter an email and password."
        
    }
    
    static let thisWeek = 0
    static let thisMonth = 1
    static let thisYear = 2
    
    static let timeLabels = ["THIS WEEK", "THIS MONTH", "THIS YEAR", "LAST WEEK", "LAST MONTH", "LAST YEAR"]

    static let weekSymbols = NSDateFormatter().shortWeekdaySymbols.map({$0.uppercaseString})
    static let monthSymbols = NSDateFormatter().shortMonthSymbols.map({$0.substringToIndex(1).uppercaseString})
    
    //Contanst for calculations based on days in...
    static let oneWeekInDays = CGFloat(7)
    static let oneYearInMonths = CGFloat(12)
    static let oneYear = CGFloat(1)
    
    //Colors
    static let MainScreenBackgroundColor = ColorUtils.colorWithHexString("#b8a599")
    
    //Time
    static var currentWeekDay: Int {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.CalendarUnitWeekday, fromDate: NSDate())
        return components.weekday
    }
    
    //Applications segues
    struct Segues {
        static let MainScreen   = "MainScreen"
        static let LoginScreen  = "Login"
    }
  
    //Parse
    struct Parse {
        struct Dev {
            
            static let ApplicationId    = "KbHIiUGQVLD9nugIXxlL0X3uzaUFlkOUqmJTCzhy"
            static let ClientKey        = "25DvY6AzY83nUMDjInEmNeTy9x8Dw9991mp2D5h7"
            
        }
        
        struct Test {
            
            static let ApplicationId    = ""
            static let ClientKey        = ""
            
        }
        
        struct Prod {
            
            static let ApplicationId    = "4kXAyuOZoJypsd2BEsKC59imVhLkHWiB0WrpS9Zu"
            static let ClientKey        = "FpOpc6IfK2yifIbZNshtoujGMYNvOq2Rs4DvaBvK"
        
        }

    }
    
    struct Jawbone {
        //Bar chart constants
        static let barChartBackgroundColor = ColorUtils.colorWithHexString("#b5a296")
        static let barChartFooterTextColor = ColorUtils.colorWithHexString("#633c02")
        static let barChartFooterTextHeight = CGFloat(16)
        static let defaultFooterPadding = CGFloat(10)
        static let maximumValue = CGFloat(100)
        static let minimumValue = CGFloat(0)
        static let defaultPaddingBetweenBars = CGFloat(2)
        static let barChartViewBarBasePaddingMutliplier = CGFloat(50)
        //bar chart view
        static let leftPaddingForBarChartView = CGFloat(10)
        static let rightPaddingForBarChartView = CGFloat(10)
    }
}
