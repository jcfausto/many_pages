//
//  MonthDataSource.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/15/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation

class MonthDataSource: MPDataSource {
    
    //Everytime that the datasource needs to be accessed, it must be done
    //by accessing this variable
    static let sharedInstance = MonthDataSource()
    
    override init() {
        super.init()
        let date = NSDate()
        self.type = NSCalendarUnit.CalendarUnitMonth
        self.queryStartDate = date.beginningOfMonth()
        self.queryEndDate = date.endOfMonth()
        
        //last month data
        self.secondaryDatasource = LastMonthDataSource()
        self.secondaryDatasource.type = self.type
    }
    
    override func initDataSource() {
        //To simplify this first version, all months will have 31 days.
        self.data = [Int](count: 31, repeatedValue: 0)
    }
    
    override func processData(objects: [AnyObject]?) {
        if (objects != nil) {
            
            var pCount: NSInteger = 0
            
            for (index, object) in enumerate(objects!) {
                
                var createdAt = object.valueForKey("createdAt") as! NSDate
                let aCalendar = NSCalendar.currentCalendar()
                let aComponents = aCalendar.components(NSCalendarUnit.CalendarUnitDay, fromDate: createdAt)
            
                pCount = object.valueForKey("page_count") as! NSInteger
                
                let dayIndex = aComponents.day-1
                
                self.setValueAtIndex(pCount, index: dayIndex)
                
            } //end for

        } //end if
    }
    
}
