//
//  UserUtils.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 3/31/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation

//General
func getUserDefaults() -> NSUserDefaults {
   return NSUserDefaults.standardUserDefaults()
}

class UserUtils {

    
    class func notLoggedIn() -> Bool {
        // here I assume that a user must be linked to Facebook
        var currentUser = PFUser.currentUser()
        
        if currentUser != nil {
            return false
        } else {
            return true
        }
    }
    
    class func loggedIn() -> Bool {
        return !notLoggedIn()
    }
    
    class func doLogout() {
        PFUser.logOut()
    }

    
    class func saveRecordForTodayWith(pageCount: NSInteger) -> Bool {
        
        if loggedIn() {
            
            if (readingRecord == nil) {
                
                readingRecord = PFObject(className:"ReadingRecord")
                
                readingRecord["user"] = PFUser.currentUser()
                readingRecord["page_count"] = 0
                
            } else {
                
                var createdAt = readingRecord.valueForKey("createdAt") as! NSDate
                let aCalendar = NSCalendar.currentCalendar()
                let aComponents = aCalendar.components(NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekday, fromDate: createdAt)
                
                let today = NSDate()
                let todayCalendar = NSCalendar.currentCalendar()
                let todayDateComponents = todayCalendar.components(NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitMonth, fromDate: today)
                
                let sameDay = (todayDateComponents.month == aComponents.month) && (todayDateComponents.day == aComponents.day)
                
                if !sameDay {
                    
                    readingRecord = PFObject(className:"ReadingRecord")
                    
                    readingRecord["user"] = PFUser.currentUser()
                    readingRecord["page_count"] = 0
                    
                }
                
            }
            
            if (readingRecord["page_count"] as! NSInteger != pageCount) {
                
                readingRecord["page_count"] = pageCount
                
                saveDataOnParse(readingRecord)
            }
            
        }
        
        return true
    }
    
    class func doRecordLoginAction() {
        
        var currentDate = NSDate()
        
        PFUser.currentUser()!.setObject(currentDate, forKey: Constants.UserLastLoginDate)
        
        saveDataOnParse(PFUser.currentUser())
        
    }
    
    class func saveDataOnParse(anObject: PFObject!) {
        
        if (anObject != nil) {
        
            anObject.saveInBackgroundWithBlock({(success, error) -> Void in
            
                if success == true {
                
                   // println("  RESULT: Data saved successfully")
                
                } else {
                
                   // println("  RESULT: Error while savind data: \(error)")
                }
            
            })
        }
        
    }
    
}

