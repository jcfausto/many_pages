//
//  MPDelegate.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/18/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation
import JBChart

class MPDelegate: NSObject, JBBarChartViewDelegate {
    
    var informationViewLabel: UILabel!
    var informationView: JBChartInformationView!
    var datasource: MPDataSource!
    
    override init() {
        super.init()
    }
    
    //Barchart customizations
    final func barChartView(barChartView: JBBarChartView!, colorForBarViewAtIndex index: UInt) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    //The required method for JBBarChartViewDelegate protocol
    func barChartView(barChartView: JBBarChartView!, heightForBarViewAtIndex index: UInt) -> CGFloat {
        return CGFloat(self.datasource.getValueAtIndex(Int(index)))
    }
    
    func updateInformationViewLabel(value: String) {
        if self.informationViewLabel != nil {
            self.informationViewLabel.text = value
        }
    }
    
    //This delegate uses a information view.
    
    func barChartView(barChartView: JBBarChartView!, didSelectBarAtIndex index: UInt, touchPoint: CGPoint) {
        
        
        //self.updateInformationViewLabel("\(Int(index)) - \(datasource.getValueAtIndex(Int(index)).description)")
        self.updateInformationViewLabel(datasource.getValueAtIndex(Int(index)).description)
        
        if self.informationView != nil {
            
            //Just to get a more smooth transition
            UIView.animateWithDuration(0.1, animations: {
                self.informationView.setHidden(false, animated: false)
                self.informationView.alpha = 1
                }, completion: nil
            )
            
        }
    }
    
    func didDeselectBarChartView(barChartView: JBBarChartView!) {
        
        self.updateInformationViewLabel("")
        
        if self.informationView != nil {
            self.informationView.setHidden(true, animated: true)
            self.informationView.alpha = 0
        }
    }
    
    func setInformationView(anInformationView: JBChartInformationView, anInformationViewLabel: UILabel) {
        self.informationViewLabel = anInformationViewLabel
        self.informationView = anInformationView
    }
    
    //End of information view methods
    
    //Default left and right padding for the barchart view
    func leftPaddingForBarChartView(barChartView: JBBarChartView!) -> CGFloat {
        return Constants.Jawbone.leftPaddingForBarChartView
    }
    
    func rightPaddingForBarChartView(barChartView: JBBarChartView!) -> CGFloat {
        return Constants.Jawbone.rightPaddingForBarChartView
    }
}
