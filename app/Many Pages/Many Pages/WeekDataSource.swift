//
//  WeekDataSource.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/12/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation

class WeekDataSource: MPDataSource {
    
    //Everytime that the datasource needs to be accessed, it must be done
    //by accessing this variable
    static let sharedInstance = WeekDataSource()
    
    override init() {
        super.init()
        let date = NSDate()
        self.type = NSCalendarUnit.CalendarUnitWeekday
        self.queryStartDate = date.beginningOfWeek()
        self.queryEndDate = date.endOfWeek()
        
        //last week data
        self.secondaryDatasource = LastWeekDataSource()
        self.secondaryDatasource.type = self.type
    }
    
    override func initDataSource() {
        self.data = [0, 0, 0, 0, 0, 0, 0]
    }
    
    override func processData(objects: [AnyObject]?) {
        
        if objects != nil {
            
            var pCount: NSInteger = 0
            
            for (index, object) in enumerate(objects!) {
                
                var createdAt = object.valueForKey("createdAt") as! NSDate
                let aCalendar = NSCalendar.currentCalendar()
                let aComponents = aCalendar.components(NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekday, fromDate: createdAt)
                
                pCount = object.valueForKey("page_count") as! NSInteger
                self.setValueAtIndex(pCount, index: aComponents.weekday-1)
                
                let today = NSDate()
                let todayCalendar = NSCalendar.currentCalendar()
                let todayDateComponents = todayCalendar.components(NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitMonth, fromDate: today)
                
                if (todayDateComponents.month == aComponents.month) && (todayDateComponents.day == aComponents.day) {
                    readingRecord = object as! PFObject
                }
                
            } // end for
            
        } //end if
    }
    
    final func getTodaysPageCount() -> NSInteger {
        if readingRecord != nil {
            return readingRecord.valueForKey("page_count") as! NSInteger
        } else {
            return 0
        }
    }
    
}
