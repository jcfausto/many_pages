//
//  MPDataSource.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/14/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation
import JBChart

class MPDataSource: NSObject, MPDataSourceProtocol, JBBarChartViewDataSource {
    
    /// required by MPDataSourceProtocol
    var type: NSCalendarUnit
    var data: [Int]
    var queryStartDate: NSDate
    var queryEndDate: NSDate
    
    /// An MPDataSource might have another data source
    var secondaryDatasource: MPDataSource!
    
    /**
        Retrieve the value of an specific record in the datasource
    */
    final func getValueAtIndex(index: Int) -> Int {
        return self.data[index]
    }
    
    /**
        Set the value of an specific record in the datasource
    */
    final func setValueAtIndex(value: NSInteger, index: Int) {
        self.data[index] = value
    }
    
    /**
        Returns the sum of all record's values in the dataset
    */
    func getTotal() -> Int {
        let addResult = self.data.reduce(0) { $0 + $1 }
        return addResult
    }
    
    /**
        Returns the quantity of records in the dataset
    */
    final func getCount() -> Int {
        if self.data.count > 0 {
            return self.data.count
        } else {
            return 1 //to avoid division by 0
        }
    }
    
    /**
        Returns the average calculated by the getTotal() / getCount()
    */
    final func getAverage() -> CGFloat {
        return CGFloat(self.getTotal()) / CGFloat(self.getCount())
    }
    
    /**
        Returns the highest value in the datasource
    */
    final func getHighestValueInDataSource() -> Int {
        let numMax: Int = self.data.reduce(Int.min, combine: { max($0, $1) })
        return numMax
    }
    
    /**
        Populate the datasource with data
    */
    func loadData() {
        
        var dsType: String = ""
        
        switch self.type {
            case NSCalendarUnit.CalendarUnitWeekday:
                dsType = "Week"
            case NSCalendarUnit.CalendarUnitMonth:
                dsType = "Month"
            case NSCalendarUnit.CalendarUnitYear:
                dsType = "Year"
            default: dsType = "Unknown"
        }
        
        let predicate = NSPredicate(format:"(createdAt >= %@) and (createdAt <= %@)", self.queryStartDate, self.queryEndDate)
        
        var query = PFQuery(className:"ReadingRecord", predicate:predicate)
        
        query.whereKey("user", equalTo:PFUser.currentUser()!)
        query.orderByAscending("createdAt")
        query.limit = 1000
        
        var objects = query.findObjects()
        
        self.processData(objects)
        
        //if another datasource is in place, then trigger the data loading in background
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INTERACTIVE.value), 0)) {
                if self.secondaryDatasource != nil {
                    self.secondaryDatasource.loadData()
                }
        }
    }

    func processData(objects: [AnyObject]?) {
        // Must be implemented in child classes
    }
    
    /// Required method for JBChar datasource protocol
    final func numberOfBarsInBarChartView(barChartView: JBBarChartView!) -> UInt {
        return UInt(self.getCount())
    }
    
    //Required by the MPDataSource protocol
    func initDataSource() {
        self.data = [0]
    }
    
    override init() {
        self.type = NSCalendarUnit.CalendarUnitYear
        self.queryStartDate = NSDate()
        self.queryEndDate = NSDate()
        self.data = [0]
        super.init()
        self.initDataSource()
    }
    
}
