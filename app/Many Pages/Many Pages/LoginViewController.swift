//
//  LoginViewController.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 3/30/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var emailField: UITextField!
    
    @IBOutlet var passwordField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            
            if PFUser.currentUser() != nil {
                
                UserUtils.doRecordLoginAction()
                
                self.goToMainScreen()
            }
            
        } else {
            
            let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                "\(Constants.ValidationMessages.NoConnectivity)", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Signup button pressed
    @IBAction func signUp(sender: AnyObject) {
        
        if emailField.text == "" || passwordField.text == "" {
            
            let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                "\(Constants.ValidationMessages.EmailOrPasswordNotInformed)", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            var activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            var newUser = PFUser()
            newUser.username = emailField.text
            newUser.password = passwordField.text
            newUser.email = emailField.text
            
            var errorMessage = "Please, try again later."
            
            newUser.signUpInBackgroundWithBlock({ (success, error) -> Void in
                
                activityIndicator.stopAnimating()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
                
                if error  == nil {
                    // Signupt successful
                    
                    //logged in
                    UserUtils.doRecordLoginAction()
                    
                    self.goToMainScreen()
                    
                } else {
                    if let errorString = error!.userInfo?["error"] as? String {
                        
                        //Display the error message
                        errorMessage = errorString
                        
                        let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                    }
                }
            
            })
            
        }
        
    }
    
    
    //Login button pressed
    @IBAction func login(sender: AnyObject) {
        
        if emailField.text == "" || passwordField.text == "" {
            
            let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                "\(Constants.ValidationMessages.EmailOrPasswordNotInformed)", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {

            self.doLogin()
       
        }
    }
    
    
    //Perform a segue to the App's Main Screen
    func goToMainScreen() {
        self.performSegueWithIdentifier(Constants.Segues.MainScreen, sender: self)
    }
    
    //Perform the facebook login for the app's user
    func doLogin() {
        
        if Reachability.isConnectedToNetwork() {
            
            var activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            var errorMessage = "Login failed."
        
            PFUser.logInWithUsernameInBackground(emailField.text, password: passwordField.text, block: { (user, error) -> Void in
                
                activityIndicator.stopAnimating()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
            
                if user != nil {
                    
                    //logged in
                    UserUtils.doRecordLoginAction()
                    
                    self.goToMainScreen()
                    
                } else {
                    
                    if let errorString = error!.userInfo?["error"] as? String {
                        
                        //Display the error message
                        errorMessage = errorString
                        
                        let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                    }
                    
                }
                
            })
            
        } else {
            
            let alertController = UIAlertController(title: "\(Constants.ManyPagesLabel)", message:
                "\(Constants.ValidationMessages.NoConnectivity)", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "\(Constants.ValidationMessages.OKIGotIt)", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func doFacebookLogin(sender: AnyObject) {
        self.doLogin()
    }
    
    // Keyboard handling routines
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.emailField.resignFirstResponder()
        self.passwordField.resignFirstResponder()
        
        return true
    }
    
}

