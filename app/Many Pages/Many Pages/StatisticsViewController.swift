//
//  SecondViewController.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 3/30/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import UIKit
import JBChart

class StatisticsViewController: UIViewController {

    @IBOutlet var lastTimeLabel: UILabel!
    @IBOutlet var thisTimeLabel: UILabel!
    @IBOutlet var thisTimeTotalLabel: UILabel!
    @IBOutlet var thisTimePagesPerDay: UILabel!
    @IBOutlet var lastTimeTotalLabel: UILabel!
    @IBOutlet var lastTimePagesPerDay: UILabel!
    
    @IBOutlet var pagesPerTimeLabel: UILabel!
    
    @IBOutlet var pagesPerTimeLastLabel: UILabel!
    
    @IBOutlet var chartView: JBBarChartView!
    @IBOutlet var timeChoices: UISegmentedControl!

    var currentDatasource: MPDataSource!
    
    var headerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Define a custom background color
        self.view.backgroundColor = Constants.MainScreenBackgroundColor
        
        //Set up the barchart component
        self.chartView.backgroundColor = Constants.Jawbone.barChartBackgroundColor
        self.chartView.footerPadding = Constants.Jawbone.defaultFooterPadding
        self.chartView.minimumValue = CGFloat(Constants.Jawbone.minimumValue)
        self.chartView.dataSource = WeekDataSource.sharedInstance
        self.chartView.delegate = WeekBarChartDelegate.sharedInstance
        self.currentDatasource = WeekDataSource.sharedInstance
        
    }
    
    final func updateCounts() {
        
        //Since users are allowed only to input integers, the sum will be and integer too.
        self.thisTimeTotalLabel.text = self.currentDatasource.getTotal().description
        self.thisTimePagesPerDay.text = NSString(format: "%.2f", self.currentDatasource.getAverage()) as String
        //Since users are allowed only to input integers, the sum will be and integer too.
        self.lastTimeTotalLabel.text = self.currentDatasource.secondaryDatasource.getTotal().description
        self.lastTimePagesPerDay.text = NSString(format: "%.2f", self.currentDatasource.secondaryDatasource.getAverage()) as String
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if Reachability.isConnectedToNetwork() {
        
            //MonthDataSource
            dispatch_async(dispatch_get_global_queue(
                Int(QOS_CLASS_USER_INTERACTIVE.value), 0)) {
                    MonthDataSource.sharedInstance.loadData()
            }
            
            //YearDataSource
            dispatch_async(dispatch_get_global_queue(
                Int(QOS_CLASS_USER_INTERACTIVE.value), 0)) {
                    YearDataSource.sharedInstance.loadData()
            }
            
            self.chartView.maximumValue = self.getMaximumValueForBarHeight()
            self.updateCounts()
            
        }
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        //The method bellow already calls reloadData()
        self.doChangeViewForThis(self.timeChoices.selectedSegmentIndex)
        //self.chartView.footerView = self.getBarChartFooterViewFor(Constants.thisWeek, barChartWidth: self.chartView.availableWidth(), barChartPaddingBetweenBars: Constants.Jawbone.defaultPaddingBetweenBars)
        self.chartView.headerView = self.getBarChartHeaderViewFor(Constants.thisWeek, barChartWidth: self.chartView.availableWidth())
        
        WeekBarChartDelegate.sharedInstance.informationViewLabel = self.headerLabel
        MonthDelegate.sharedInstance.informationViewLabel = self.headerLabel
        YearDelegate.sharedInstance.informationViewLabel = self.headerLabel

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getBarChartHeaderViewFor(time: Int, barChartWidth: CGFloat) -> UIView {
        //Bar chart header
        headerLabel = UILabel(frame: CGRectMake(0, 0, barChartWidth, 50))
        
        headerLabel.textColor = Constants.Jawbone.barChartFooterTextColor
        
        headerLabel.font = UIFont.systemFontOfSize(15)
        
        headerLabel.textAlignment = NSTextAlignment.Center
        
        return headerLabel as UIView
    }
    
    
    func getBarChartFooterViewFor(time: Int, barChartWidth: CGFloat, barChartPaddingBetweenBars: CGFloat) -> UIView {
        
        var footerView = UIView(frame: CGRectMake(0, 0, barChartWidth, 16))
        
        let yPosition = 0
        
        //This is the x-position relative to the footerView, and not to the barChartView
        var xOffset: CGFloat = 0
        
        var barPadding: CGFloat!
        let footerLabelWidth: CGFloat!
        
        switch time {
            case Constants.thisWeek:
                barPadding =  ((1/7) * 50)
                footerLabelWidth = ((barChartWidth-(6*barPadding))/CGFloat(7))
                for weekDay in 1...7 {
                    var footerLabel = UILabel(frame: CGRectMake(xOffset, 0, footerLabelWidth, Constants.Jawbone.barChartFooterTextHeight))
                    footerLabel.textColor = UIColor.whiteColor()
                    footerLabel.text = Constants.weekSymbols[weekDay-1] as String
                    footerLabel.textAlignment = NSTextAlignment.Center
                    footerLabel.font = UIFont.systemFontOfSize(12)
                    footerLabel.textColor = Constants.Jawbone.barChartFooterTextColor
                    
                    footerView.addSubview(footerLabel)
                    
                    xOffset = CGRectGetMaxX(footerLabel.frame) + barPadding
                }
            case Constants.thisMonth:
                footerLabelWidth = (barChartWidth/2)
                var leftFooterLabel = UILabel(frame: CGRectMake(xOffset, 0, footerLabelWidth, Constants.Jawbone.barChartFooterTextHeight))
                var rightFooterLabel = UILabel(frame: CGRectMake(footerLabelWidth, 0, footerLabelWidth, Constants.Jawbone.barChartFooterTextHeight))
                leftFooterLabel.textColor = UIColor.whiteColor()
                rightFooterLabel.textColor = UIColor.whiteColor()
                
                leftFooterLabel.text = Constants.FirstDayLabel
                rightFooterLabel.text = Constants.LastDayLabel
                leftFooterLabel.textAlignment = NSTextAlignment.Left
                rightFooterLabel.textAlignment = NSTextAlignment.Right
                leftFooterLabel.font = UIFont.systemFontOfSize(12)
                rightFooterLabel.font = UIFont.systemFontOfSize(12)
                leftFooterLabel.textColor = Constants.Jawbone.barChartFooterTextColor
                rightFooterLabel.textColor = Constants.Jawbone.barChartFooterTextColor
                
                footerView.addSubview(leftFooterLabel)
                footerView.addSubview(rightFooterLabel)
            
            case Constants.thisYear:
                barPadding =  ((1/12) * 50)
                footerLabelWidth = ((barChartWidth-(11*barPadding))/CGFloat(12))
                for month in 1...12 {
                    var footerLabel = UILabel(frame: CGRectMake(xOffset, 0, footerLabelWidth, Constants.Jawbone.barChartFooterTextHeight))
                    footerLabel.textColor = UIColor.whiteColor()
                    footerLabel.text = Constants.monthSymbols[month-1] as String
                    footerLabel.textAlignment = NSTextAlignment.Center
                    footerLabel.font = UIFont.systemFontOfSize(12)
                    footerLabel.textColor = Constants.Jawbone.barChartFooterTextColor
                    
                    footerView.addSubview(footerLabel)
                    
                    xOffset = CGRectGetMaxX(footerLabel.frame) + barPadding
                }
            default:
                barPadding = 0
                footerLabelWidth = barChartWidth
        }
        
        return footerView
        
    }

    
    @IBAction func timeChanged(sender: AnyObject) {
        self.doChangeViewForThis(self.timeChoices.selectedSegmentIndex)
    }
    
    final func getMaximumValueForBarHeight() -> CGFloat {
        return CGFloat(self.currentDatasource.getHighestValueInDataSource() + 10)
    }
    
    final func setupWeekChart() {
        self.pagesPerTimeLabel.text = "PAGES / DAY"
        self.pagesPerTimeLastLabel.text = "PAGES / DAY"
        self.thisTimeLabel.text = Constants.timeLabels[Constants.thisWeek]
        self.lastTimeLabel.text = Constants.timeLabels[3+Constants.thisWeek]
        self.chartView.dataSource = WeekDataSource.sharedInstance
        self.chartView.delegate = WeekBarChartDelegate.sharedInstance
        self.currentDatasource = WeekDataSource.sharedInstance
        self.chartView.maximumValue = self.getMaximumValueForBarHeight()
        //The method bellow already calls reloadData()
        self.chartView.footerView = self.getBarChartFooterViewFor(Constants.thisWeek, barChartWidth: self.chartView.availableWidth(), barChartPaddingBetweenBars: Constants.Jawbone.defaultPaddingBetweenBars)
        self.updateCounts()
    }
    
    final func setupMonthChart() {
        self.pagesPerTimeLabel.text = "PAGES / DAY"
        self.pagesPerTimeLastLabel.text = "PAGES / DAY"
        self.thisTimeLabel.text = Constants.timeLabels[Constants.thisMonth]
        self.lastTimeLabel.text = Constants.timeLabels[3+Constants.thisMonth]
        self.chartView.dataSource = MonthDataSource.sharedInstance
        self.currentDatasource = MonthDataSource.sharedInstance
        self.chartView.delegate = MonthDelegate.sharedInstance
        self.chartView.maximumValue = self.getMaximumValueForBarHeight()
        //The method bellow already calls reloadData()
        self.chartView.footerView = self.getBarChartFooterViewFor(Constants.thisMonth, barChartWidth: self.chartView.availableWidth(), barChartPaddingBetweenBars: Constants.Jawbone.defaultPaddingBetweenBars)
        self.updateCounts()
    }
    
    
    final func setupYearChart() {
        self.pagesPerTimeLabel.text = "PAGES / MONTH"
        self.pagesPerTimeLastLabel.text = "PAGES / MONTH"
        self.thisTimeLabel.text = Constants.timeLabels[Constants.thisYear]
        self.lastTimeLabel.text = Constants.timeLabels[3+Constants.thisYear]
        self.chartView.dataSource = YearDataSource.sharedInstance
        self.currentDatasource = YearDataSource.sharedInstance
        self.chartView.delegate = YearDelegate.sharedInstance
        self.chartView.maximumValue = self.getMaximumValueForBarHeight()
        //The method bellow already calls reloadData()
        self.chartView.footerView = self.getBarChartFooterViewFor(Constants.thisYear, barChartWidth: self.chartView.availableWidth(), barChartPaddingBetweenBars: Constants.Jawbone.defaultPaddingBetweenBars)
        self.updateCounts()
    }
    
    
    final func doChangeViewForThis(time: Int) {
        
        switch time {
            case Constants.thisWeek:
                self.setupWeekChart()
            case Constants.thisMonth:
                self.setupMonthChart()
            case Constants.thisYear:
                self.setupYearChart()
            default: Constants.thisWeek
        }
        
    }
    


}

