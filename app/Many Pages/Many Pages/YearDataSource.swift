//
//  YearDataSource.swift
//  Many Pages
//
//  Created by Julio Cesar Fausto on 4/15/15.
//  Copyright (c) 2015 Julio Cesar Fausto. All rights reserved.
//

import Foundation

class YearDataSource: MPDataSource {
    
    //Everytime that the datasource needs to be accessed, it must be done
    //by accessing this variable
    static let sharedInstance = YearDataSource()
    
    override init() {
        super.init()
        let date = NSDate()
        self.type = NSCalendarUnit.CalendarUnitYear
        self.queryStartDate = date.beginningOfYear()
        self.queryEndDate = date.endOfYear()
        
        //last year data
        self.secondaryDatasource = LastYearDataSource()
        self.secondaryDatasource.type = self.type

    }
    
    override func initDataSource() {
        // the data will be grouped by month
        self.data = [Int](count: 12, repeatedValue: 0)
    }
    
    override func processData(objects: [AnyObject]?) {
        if (objects != nil) {
            
            var pCount: NSInteger = 0
            var currentMonth: Int = 1
            
            for (index, object) in enumerate(objects!) {
                var createdAt = object.valueForKey("createdAt") as! NSDate
                let aCalendar = NSCalendar.currentCalendar()
                let aComponents = aCalendar.components(NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekday, fromDate: createdAt)
                let lastObject = (objects!.endIndex-1 == index)
            
                if (currentMonth == aComponents.month) {
                    pCount += object.valueForKey("page_count") as! NSInteger
                } else {
                    //set the acumulated count for the previous month
                    self.setValueAtIndex(pCount, index: currentMonth-1)
                    //reset the count
                    pCount = object.valueForKey("page_count") as! NSInteger
                    currentMonth = aComponents.month
                }
                
                if (lastObject) {
                    //set the acumulated count for the previous month
                    self.setValueAtIndex(pCount, index: currentMonth-1)
                }
                
            } //end for
        } //end if
    }
    
}